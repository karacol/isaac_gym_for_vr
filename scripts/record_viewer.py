import rospy
import numpy as np
import os
import time
from PIL import Image
from sensor_msgs.msg import CompressedImage
import cv2


class ImageWriter(object):
    def __init__(self) -> None:
        rospy.Subscriber('/IsaacGym/image/compressed', CompressedImage, self.write, queue_size=1)
        self.frame_no = 0
        image_params = rospy.get_param('/image')
        self.width = image_params['width']
        self.height = image_params['height']
        self.dir = f"collect_demo_viewer_{int(time.time())}"
        os.mkdir(self.dir)
        self.fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        self.video_left =cv2.VideoWriter(
                    os.path.join(self.dir, "viewer_recording_left.mp4"),
                    self.fourcc, 90,
                    (1000, 900))
        self.video_right =cv2.VideoWriter(
                    os.path.join(self.dir, "viewer_recording_right.mp4"),
                    self.fourcc, 90,
                    (1000, 900))

    def write(self, image_msg):
        data_bytes = image_msg.right.data
        data_arr = np.frombuffer(data_bytes, np.uint8).reshape((self.height, self.width, 3))
        data_arr_flipped = np.flip(data_arr, axis=0)
        # write video
        self.video_right.write(data_arr_flipped[:, :, ::-1])
        data_bytes = image_msg.left.data
        data_arr = np.frombuffer(data_bytes, np.uint8).reshape((self.height, self.width, 3))
        data_arr_flipped = np.flip(data_arr, axis=0)
        # write video
        self.video_left.write(data_arr_flipped[:, :, ::-1])


if __name__ == "__main__":
    writer = ImageWriter()
    rospy.init_node('WriteViewer')
    try:
        rospy.spin()
    except KeyboardInterrupt:
        writer.video.release()
        print("Shutting down.")
