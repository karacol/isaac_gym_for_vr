#!/usr/bin/env python3

"""
Example IsaaacGym simulation using VRViewer
"""

import numpy as np
import os
import rospy
from omegaconf import OmegaConf
from isaacgym import gymapi
from vrviewer import ExampleEnv, VRViewer
from vrviewer.utils import vec32dict, quat2dict

y_up = False

example = ExampleEnv(y_up=y_up, headless=True)

if y_up:
    axis_up = 'Y'
    hmd_pos = gymapi.Vec3(50, 2, 53)
    hmd_rot = gymapi.Quat.from_axis_angle(gymapi.Vec3(0, 1, 0), np.deg2rad(90))
    override_configs = {'axis_up': axis_up, 'hmd_pos': vec32dict(hmd_pos), 'hmd_rot': quat2dict(hmd_rot)}
else:
    override_configs = None

cfg = OmegaConf.load(os.path.join(os.path.dirname(__file__), '../cfg/config.yaml'))
vr_viewer = VRViewer(example.gym, example.env, example.sim, cfg.vrviewer, override_configs)

# run simulation
running = True
while running and vr_viewer.alive:
    try:
        vr_viewer.step()
        running = example.step()
    except rospy.ROSInterruptException:
        pass
