# Isaac Gym for VR

This is a ROS approach to enabling a VR viewer for Isaac Gym simulations.
It makes use of the [Head and Hand Tracker](https://git.ais.uni-bonn.de/mosbach/head_and_hand_tracker) package.

## Requirements

- ROS
- Isaac Gym
- OmegaConf

I suggest setting up a `conda` environment like this:

```bash
conda create -n ros-ig ros-noetic-desktop python=3.8 -c robostack -c robostack-experimental -c conda-forge --no-channel-priority --override-channels
conda env update -n ros-ig --file docs/isaacgym_deps.yaml
conda activate ros-ig
cd ${ISAAC_GYM_PATH}/python
pip install -e .
conda install -c conda-forge omegaconf
```

where you fill in your path to Isaac Gym.

Also, in `~/.bashrc`, comment out `source /opt/ros/noetic/setup.bash` as this will conflict with the `conda` install of ROS.

If you do not want ROS to run inside `conda`, you need to make Isaac Gym available outside a `conda` environment.

## Installation

In your catkin workspace, `cd src` and clone this code.

Run `catkin_make` in your catkin workspace and then `source devel/setup.bash`.

## Usage

You can adjust the factor that will be applied to the recommended image width and height in [`param.yaml`](param.yaml).

Launch the Head and Hand Tracker in a terminal.

In another terminal, run `roslaunch isaac_gym_for_vr isaac_gym_for_vr.launch`.

In another terminal, run `rosrun isaac_gym_for_vr example.py` or your own simulation.

### Own simulation

To run your own simulation, simply create a `VRViewer` instance in your program and call its `step` function in the main loop:

```py
import rospy
from vrviewer import VRViewer
from omegaconf import OmegaConf

# initialise Gym, your simulation, ...

cfg = OmegaConf.load('cfg/config.yaml')
vr_viewer = VRViewer(gym, env, sim, cfg.vrviewer)
while True:
    try:
        alive = vr_viewer.step()
        # also step your simulation here
    except rospy.ROSInterruptException:
        pass
```

`VRViewer` expects a configuration file in `yaml` format that looks like [`config.yaml`](cfg/config.yaml).
You can also override its contents when creating an instance.
See [`example.py`](scripts/example.py) for more details. It uses the simulation specified in [`ExampleEnv`](src/vrviewer/example_env.py).