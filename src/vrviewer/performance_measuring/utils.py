"""
Collection of useful functions for measuring the performance of different components of VRViewer.
"""

import time
import json
import numpy as np
from omegaconf import OmegaConf

performance_test = True
path = "/home/user/moraw/catkin_ws/src/isaac_gym_for_vr/src/vrviewer/"

time_dict = {}

def parse_performance_config():
    conf_path = path + "performance_config.yaml"
    perf_conf = OmegaConf.load(conf_path)
    s = perf_conf.scenario
    drop_alpha = perf_conf[s].drop_alpha
    use_view = perf_conf[s].use_view
    camera_tensors = perf_conf[s].camera_tensors
    return s, drop_alpha, use_view, camera_tensors

def measure_performance(func):
    def wrap_func(*args, **kwargs):
        if not func.__name__ in time_dict:
            time_dict[func.__name__] = []
        start = time.perf_counter_ns()
        result = func(*args, **kwargs)
        end = time.perf_counter_ns()
        time_dict[func.__name__].append(end-start)
        return result
    return wrap_func

def mean_performance():
    if len(time_dict["render_all_camera_sensors"])%100 == 0:
        means = {}
        for k, v in time_dict.items():
            means[k] = np.mean(v)
        sum = means["__publish_image"]
        print("Mean performance per function:")
        for k, v in means.items():
            print(f"    {k}: {v:.2f} ns ({v/sum*100:.2f}%)")
        print(f"    Total: {sum:.2f} ns")

def write_performance(self):
    filename = path + 'performance.json'
    try:
        f = open(filename, "r+")
        data = json.load(f)
        f.seek(0)
    except FileNotFoundError:
        f = open(filename, "w")
        data = {"scenarios": []}
        for _ in range(4):
            scenario = {"runs": []}
            data["scenarios"].append(scenario)
    data["scenarios"][self.scenario]["runs"].append(time_dict)
    json.dump(data, f)    