#!/bin/bash

# to run: screen -S performance_test; ./run_performance.sh > performance_test.log 2>&1; CTRL+A CTRL+D
# to reattach: screen -r

run_info_str="Starting Run "
scenario_info_str="Starting Scenario "
config_path=/home/user/moraw/catkin_ws/src/isaac_gym_for_vr/src/vrviewer/performance_config.yaml

# run roscore before

source /home/user/moraw/miniconda3/etc/profile.d/conda.sh
conda activate ros-ig
source /home/user/moraw/catkin_ws/devel/setup.bash

rosparam load /home/user/moraw/catkin_ws/src/isaac_gym_for_vr/param.yaml
nohup rosbag play -l -q --wait-for-subscribers /home/user/moraw/catkin_ws/bagfiles/hmd_0_pose.bag &
nohup rosbag play -l -q --wait-for-subscribers /home/user/moraw/catkin_ws/bagfiles/handstate_right.bag &

cd /home/user/moraw/gym-grasp/gym_grasp
wandb disabled

for s in {0..3}; do
    sed -i "s/^scenario: ./scenario: $s/" $config_path
    echo $scenario_info_str$s
    for n in {1..10}; do
        echo $run_info_str$n
        python3 gym_grasp_demo.py task=LiftObject headless=True rl_device=cpu sim_device=cpu num_envs=1
    done
done