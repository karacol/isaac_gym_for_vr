def vec32dict(vec):
    """Returns a dict() representation of isaacgym.gymapi.Vec3."""
    return {'x': vec.x, 'y': vec.y, 'z': vec.z}

def quat2dict(quat):
    """Returns a dict() representation of isaacgym.gymapi.Quat."""
    return {'x': quat.x, 'y': quat.y, 'z': quat.z, 'w': quat.w}