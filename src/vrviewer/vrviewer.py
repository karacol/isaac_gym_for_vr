#!/usr/bin/env python3

import rospy
import numpy as np
import io
from geometry_msgs.msg import PoseStamped
from isaac_gym_for_vr.msg import EyeImages
from isaacgym import gymapi, gymtorch
from omegaconf import OmegaConf
import torch
import time

class VRViewer(object):
    """VRViewer class to read tracking data and have one sensor camera per eye follow it.
    """
    def __init__(self, gym, env, sim, conf, override_configs=None):
        """Constructor method. Creates the headset actor and starts the tracking process.

        Args:
            gym (isaacgym.gymapi.Gym): Gym singleton
            env (isaacgym.gymapi.Env): environment
            sim (isaacgym.gymapi.Sim): simulation handle
            conf (dict): configuration
            override_configs (dict): dictionary to overide configuration in the config file, optional
        """
        self.gym = gym
        self.env = env
        self.sim = sim
        # load config
        if override_configs is not None:
            for k, v in override_configs.items():
                conf[k] = v
        self.hmd_pos = gymapi.Vec3(**conf["hmd_pos"])
        self.hmd_rot = gymapi.Quat(**conf["hmd_rot"])
        self.axis_up = conf["axis_up"]
        self.init_ros = conf["init_ros"]
        self.camera_tensors = conf["camera_tensors"]
        # performance
        #if performance_test:
        #    self.scenario, self.drop_alpha, self.use_view, self.camera_tensors = parse_performance_config()        
        # prepare camera
        if self.axis_up == 'Y':
            self.camera_rotation = gymapi.Quat.from_axis_angle(gymapi.Vec3(0, 1, 0), np.deg2rad(0))
        elif self.axis_up == 'Z':  # need to take extra steps as OpenVR has Y_AXIS_UP and reports the pose accordingly
            self.hmd_correction = gymapi.Quat.from_axis_angle(gymapi.Vec3(1, 0, 0), np.deg2rad(90))
            camera_correction = gymapi.Quat.from_axis_angle(gymapi.Vec3(1, 0, 0), np.deg2rad(-90))
            self.camera_rotation = camera_correction * gymapi.Quat.from_axis_angle(gymapi.Vec3(0, 0, 1), np.deg2rad(90))
        else:
            raise ValueError("Must be 'Y' or 'Z'.")
        # ROS
        if self.init_ros:
            rospy.init_node('VRSim')
        self.pub = rospy.Publisher('/IsaacGym/images/compressed', EyeImages, queue_size=1)
        # prepare camera
        self.__create_cameras()
        rospy.Subscriber('/hmd/0/pose', PoseStamped, self.__move_cameras, queue_size=10)
        self.fps_dict = self.init_fps_report()
        self.alive = True

    def __create_cameras(self):
        """Creates two sensor cameras for both eyes."""
        # get image width and height
        ready = rospy.has_param('/resolution/height')
        print("Waiting for parameters from OpenVR.", end="")
        while not ready:
            print(".", end="")
            time.sleep(0.1)
            ready = rospy.has_param('/resolution/height')
        print(" Done.")
        image_params = rospy.get_param('/resolution')
        eye_params = rospy.get_param('/eye')
        camera_props = gymapi.CameraProperties()
        if self.camera_tensors:
            camera_props.enable_tensors = True
        camera_props.width = image_params['width']
        camera_props.height = image_params['height']
        camera_props.horizontal_fov = int(np.rad2deg(eye_params['left']['fov_in_rad']))
        self.camera_handle_left = self.gym.create_camera_sensor(self.env, camera_props)
        camera_props.horizontal_fov = int(np.rad2deg(eye_params['right']['fov_in_rad']))
        self.camera_handle_right = self.gym.create_camera_sensor(self.env, camera_props)
        # set up HMD
        hmd_transform = gymapi.Transform()
        hmd_transform.p = self.hmd_pos
        if self.axis_up == 'Y':
            hmd_transform.r = self.hmd_rot
        else:
            hmd_transform.r = self.hmd_rot * self.hmd_correction
        # apply initial transformation
        self.camera_offset_left = gymapi.Vec3(eye_params['left']['x'], eye_params['left']['y'], eye_params['left']['z'])
        self.camera_offset_right = gymapi.Vec3(eye_params['right']['x'], eye_params['right']['y'], eye_params['right']['z'])
        cam_pos_left = hmd_transform.transform_point(self.camera_offset_left)
        cam_pos_right = hmd_transform.transform_point(self.camera_offset_right)
        cam_rot = hmd_transform.r * self.camera_rotation
        cam_transform_left = gymapi.Transform(cam_pos_left, cam_rot)
        cam_transform_right = gymapi.Transform(cam_pos_right, cam_rot)
        self.gym.set_camera_transform(self.camera_handle_left, self.env, cam_transform_left)
        self.gym.set_camera_transform(self.camera_handle_right, self.env, cam_transform_right)
        if self.camera_tensors:
            # get tensors
            self.camera_tensor_left = self.gym.get_camera_image_gpu_tensor(self.sim, self.env, self.camera_handle_left, gymapi.IMAGE_COLOR)
            self.torch_camera_tensor_left = gymtorch.wrap_tensor(self.camera_tensor_left)
            self.camera_tensor_right = self.gym.get_camera_image_gpu_tensor(self.sim, self.env, self.camera_handle_right, gymapi.IMAGE_COLOR)
            self.torch_camera_tensor_right = gymtorch.wrap_tensor(self.camera_tensor_right)

    def __move_cameras(self, pose_msg):
        if self.axis_up == 'Y':
            pos = gymapi.Vec3(pose_msg.pose.position.x, pose_msg.pose.position.y, pose_msg.pose.position.z) + self.hmd_pos
            rot = self.hmd_rot * gymapi.Quat(pose_msg.pose.orientation.x, pose_msg.pose.orientation.y, pose_msg.pose.orientation.z, pose_msg.pose.orientation.w)
        else:
            # switch z and y in position
            pos = gymapi.Vec3(pose_msg.pose.position.x, -pose_msg.pose.position.z, pose_msg.pose.position.y) + self.hmd_pos
            # map x->x, y->z, z->-y, apply correction for the hmd actor body to be correctly oriented
            rot = self.hmd_rot * gymapi.Quat(pose_msg.pose.orientation.x, -pose_msg.pose.orientation.z, pose_msg.pose.orientation.y, pose_msg.pose.orientation.w) * self.hmd_correction
        hmd_transform = gymapi.Transform(pos, rot)
        cam_pos_left = hmd_transform.transform_point(self.camera_offset_left)
        cam_pos_right = hmd_transform.transform_point(self.camera_offset_right)
        cam_rot = hmd_transform.r * self.camera_rotation
        cam_transform_left = gymapi.Transform(cam_pos_left, cam_rot)
        cam_transform_right = gymapi.Transform(cam_pos_right, cam_rot)
        self.gym.set_camera_transform(self.camera_handle_left, self.env, cam_transform_left)
        self.gym.set_camera_transform(self.camera_handle_right, self.env, cam_transform_right)

    def __publish_image(self):
        self.gym.render_all_camera_sensors(self.sim)
        msg = EyeImages()
        msg.left.header.stamp = rospy.Time.now()
        msg.right.header.stamp = rospy.Time.now()
        #msg.format = "png"
        if self.camera_tensors:  # image shape (height, width, 4)
            self.gym.start_access_image_tensors(self.sim)
            # remove alpha channel, flip s.t. (0, 0) is in bottom left corner as expected by OpenGL
            image_left = self.torch_camera_tensor_left[:,:,:3].flip([0])
            image_right = self.torch_camera_tensor_right[:,:,:3].flip([0])
            msg.left.data = image_left.cpu().numpy().tobytes()
            msg.right.data = image_right.cpu().numpy().tobytes()
            self.pub.publish(msg)
            self.gym.end_access_image_tensors(self.sim)    
        else:  # image shape (height, width*4)
            image_left = self.gym.get_camera_image(self.sim, self.env, self.camera_handle_left, gymapi.IMAGE_COLOR)
            image_right = self.gym.get_camera_image(self.sim, self.env, self.camera_handle_right, gymapi.IMAGE_COLOR)
            # remove alpha channel (is always 255)
            no_alpha_left = image_left.view()
            no_alpha_left.shape = (image_left.shape[0], -1, 4)
            no_alpha_right = image_right.view()
            no_alpha_right.shape = (image_right.shape[0], -1, 4)
            # flip s.t. (0, 0) is in bottom left corner as expected by OpenGL
            image_left = np.flip(no_alpha_left[:,:,:3], axis=0)
            image_right = np.flip(no_alpha_right[:,:,:3], axis=0)
            msg.left.data = image_left.tobytes()
            msg.right.data = image_right.tobytes()
            self.pub.publish(msg)
            
    def init_fps_report(self):
        now = time.time()
        d = {"frame_count": 0, "next_fps_report": now+2.0, "fps_t_start": now}
        return d

    def report_fps(self, d):
        t = time.time()
        if t >= d["next_fps_report"]:
            fps_t_end = time.time()
            print(f"FPS (vr viewer): {d['frame_count'] / (fps_t_end - d['fps_t_start'])}")
            d["frame_count"] = 0
            d["fps_t_start"] = time.time()
        d["next_fps_report"] = d["fps_t_start"] + 2.0
            

    def step(self):  # muss bis 1/90 sleepen
        self.fps_dict["frame_count"] += 1
        self.report_fps(self.fps_dict)
        if not rospy.is_shutdown():
            self.__publish_image()
            self.alive = True
        else:
            self.alive = False

