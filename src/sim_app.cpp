#include "sim_app.h"

sim_app::sim_app(double factor, int argc, char **argv)
{
    vr::EVRInitError error;
    
    vr_system = vr::VR_Init(&error, vr::VRApplication_Scene);
    if (error != vr::VRInitError_None){
        vr_system = NULL;
        std::cout << "Unable to initialise VR runtime: " << vr::VR_GetVRInitErrorAsEnglishDescription(error) << "\n";
        exit(EXIT_FAILURE);
    }
    
    vr::HmdMatrix34_t transform_left = vr_system->GetEyeToHeadTransform(vr::Eye_Left);
    vr::HmdMatrix34_t transform_right = vr_system->GetEyeToHeadTransform(vr::Eye_Right);

    vr::HmdMatrix44_t projection_left = vr_system->GetProjectionMatrix(vr::Eye_Left, 0.1f, 30.0f);
    vr::HmdMatrix44_t projection_right = vr_system->GetProjectionMatrix(vr::Eye_Right, 0.1f, 30.0f);    
    
    float d_left = transform_left.m[0][3];
    float d_right = transform_right.m[0][3];

    float fov_left = fabs(atan(projection_left.m[2][2] / projection_left.m[1][1]))*2.0;
    float fov_right = fabs(atan(projection_right.m[2][2] / projection_right.m[1][1]))*2.0;
    
    eye_left["x"] = transform_left.m[0][3];
    eye_left["y"] = transform_left.m[1][3];
    eye_left["z"] = transform_left.m[2][3];
    eye_left["fov_in_rad"] = fov_left;
    eye_right["x"] = transform_right.m[0][3];
    eye_right["y"] = transform_right.m[1][3];
    eye_right["z"] = transform_right.m[2][3];
    eye_right["fov_in_rad"] = fov_right;

    uint32_t recommended_width;
    uint32_t recommended_height;
    
    vr_system->GetRecommendedRenderTargetSize(&recommended_width, &recommended_height);
    
    width = int(recommended_width * factor);
    height = int(recommended_height * factor);
    // increase to even pixel numbers
    width = width - (width % 100);
    height = height - (height % 100);
    
    // initialise window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Sim_App");
    glutHideWindow();

    // initialise GLEW, check for errors
    GLenum res = glewInit();
    if (res != GLEW_OK){
        std::cout << "GLEW initialisation error: " << glewGetErrorString(res) << "\n";
        exit(EXIT_FAILURE);
    }
    
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glGenTextures(1, &left);
    glGenTextures(1, &right);

    glFinish();
}

sim_app::~sim_app()
{
    if (vr_system != NULL){
        vr::VR_Shutdown();
        vr_system = NULL;
    }
    glDeleteTextures(1, &left);
    glDeleteTextures(1, &right);
}

void sim_app::check_compositor_error(){
    if (compositor_error != vr::EVRCompositorError::VRCompositorError_None){
        vr_system = NULL;
        std::cout << "Unable to manipulate compositor: " << static_cast<int>(compositor_error) << "\n";
        exit(EXIT_FAILURE);        
    }
}

void sim_app::render(){
    vr::TrackedDevicePose_t pose[vr::k_unMaxTrackedDeviceCount];
    compositor_error = vr::VRCompositor()->WaitGetPoses(pose, vr::k_unMaxTrackedDeviceCount, NULL, 0);
    check_compositor_error();

    vr::Texture_t left_eye_tex;
    left_eye_tex.handle = (void*)(uintptr_t) left;
    left_eye_tex.eColorSpace = vr::ColorSpace_Linear;
	left_eye_tex.eType = vr::TextureType_OpenGL;
    compositor_error = vr::VRCompositor()->Submit(vr::Eye_Left, &left_eye_tex);
    check_compositor_error();

    vr::Texture_t right_eye_tex;
    right_eye_tex.handle = (void*)(uintptr_t) right;
    right_eye_tex.eColorSpace = vr::ColorSpace_Linear;
	right_eye_tex.eType = vr::TextureType_OpenGL;
    compositor_error = vr::VRCompositor()->Submit(vr::Eye_Right, &right_eye_tex);
    check_compositor_error();

    glFinish();
}

void sim_app::callback(const isaac_gym_for_vr::EyeImages msg){
    buf_left = const_cast<uint8_t*>(msg.left.data.data());
    buf_right = const_cast<uint8_t*>(msg.right.data.data());

    glBindTexture(GL_TEXTURE_2D, left);    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, buf_left);
    std::cout << "Manipulating OpenGL texture: " << glGetError() << "\n";

    glBindTexture(GL_TEXTURE_2D, right);    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, buf_right);
    std::cout << "Manipulating OpenGL texture: " << glGetError() << "\n";

    render();
}

void sim_app::set_params(ros::NodeHandle nh){
    nh.setParam("/eye/left", eye_left);
    nh.setParam("/eye/right", eye_right);
    nh.setParam("/resolution/width", width);
    nh.setParam("/resolution/height", height);
}
