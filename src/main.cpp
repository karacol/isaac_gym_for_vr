/*
OpenVR application for tracking HMD.
Compile using cmake.
*/

#include <chrono>
#include <thread>
#include <csignal>
#include "sim_app.h"

int main(int argc, char **argv){
    ros::init(argc, argv, "isaacgym_viewer");
    ros::NodeHandle nh;
    double resolution_factor;
    nh.getParam("/resolution/factor", resolution_factor);
    sim_app app(resolution_factor, argc, argv);
    app.set_params(nh);
    ros::Subscriber sub = nh.subscribe("/IsaacGym/images/compressed", 1, &sim_app::callback, &app);
    ros::spin();
    return 0;
}
