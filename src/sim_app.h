#ifndef SIM_APP_H
#define SIM_APP_H

#include <openvr.h>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "ros/ros.h"
#include "isaac_gym_for_vr/EyeImages.h"

class sim_app
{
private:
    vr::IVRSystem* vr_system = NULL;
    vr::EVRCompositorError compositor_error;
    vr::TrackedDeviceIndex_t hmd_id;
    GLuint left;
    GLuint right;
    int width = 0;
    int height = 0;
    uint8_t* buf_left = NULL;
    uint8_t* buf_right = NULL;
    std::map<std::string, double> eye_left;
    std::map<std::string, double> eye_right;
    void check_compositor_error();
    void render();
public:
    sim_app(double factor, int argc, char **argv);
    ~sim_app();
    void callback(const isaac_gym_for_vr::EyeImages msg);
    void set_params(ros::NodeHandle nh);
};

#endif